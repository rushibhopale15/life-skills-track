# Energy Management


## 1. What are the activities you do that make you relax - Calm quadrant?

- Listening music.
- Playing cricket or football
- Calling and talking to my mom and dad or friend and have a long conversation.
- Finally, having naps on a stressful day is quiet calming.

## 2. When do you find getting into the Stress quadrant?

- When things don't go as planned and you feel like things are about to fall apart, that situtation can give me a headache.
- When the sudden review started.
- When the deadline is closed, my work is not completed.

## 3. How do you understand if you are in the Excitement quadrant?

- When I achieve my goals.
- When I complete my assignment task within time.
- When the review is going well.

## 4. Paraphrase the Sleep is your Superpower video in detail

 Matt Walker, the speaker, starts by talking about how lack of sleep can cause a reduction in the level of male and female hormones, which can not only cause aging faster but also impact the reproductive health. Matt, then continues with how a study suggests that for retaining newly learned information, we need to sleep not only before studying but also after studying, to prepare our brains to absorb new information.

 The speaker then moves on to how sleep depravation reduces the performance of your brain by 40%, in a data collected from two group in which one was sleep deprived and the other has seven and a half hours of sleep. There is also a reduction in brain activity in the individuals who got sufficient amount of sleep.

 Matt mentions how during sleep there a sudden burst of electrical signals, called sleep-spindles, which is pathway to a more permanent long-term storage, just like how memory is offloaded from a RAM to a ROM, when user exits an application.

 Aging is one of the important factors that contributes to memory declines, but  sleep deprivation is another factor that largely get ignored.

 Matt's Sleep center is not to optimistic about sleeping pills. His center uses another method of giving electric shock with unnoticeable amount of voltage that we can't feel.

 Matt then associates how day-light savings time in US at spring time affects individuals, where there is a 24% increase in heart related hospitalizations, due to reduction in sleeping time and a 24% decrease during summer, when people get adequate amount of sleep. He also talks about there being a correlation between day-light savings time and car accidents which are majorly caused because of lack of sleep.

 Getting just 4 hours of sleep a day, there is a 70% decrease in the number of natural killer cells in your body. He then links this to how it is reponsible for various cancers. WHO has called a night shift work a probable carcinogen, because of the said reasons.

Matt lists tips for better sleep; regularity (go to bed at same time and wake up at the same time), being in cool environment (it allows our body to sleep better when we experience a reduction in surround temperature by 2 degree Celcius.)


## 5. What are some ideas that you can implement to sleep better?

Enumerated are few tips to sleep better:

- being regular and going to bed at the same time and getting up at the same time

- only using bed for sleep so that our brain makes association of our bed with sleep

- sleeping in a room which is 2 degree celcius cooler

- avoiding large meals before bed

- getting exercise during the day


## 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points

Wendy Suzuki starts by talking about how physical activity can have a long-lasting positive impact on one's well-being.
She tells about the brain structure; the front part is called the prefrontal cortex, which is responsible for decision-making.
We also have two temporal lobes on each side of our brain. This part of the brain is responsible for remembering events and memories.
Hippocampus is another part of the brain. Here permanent memories are stored.
She tells about her being sedentary during her time in research, how she felt unhappy and unsatisfied, and also gained 50 pounds.
After that, she started exercising and doing different activities to lose weight, enjoyed the process, and learn how important to exercise regularly to maintain a good mood the whole day.
It was then that she enjoyed doing her work even more. It was like she was researching other people, but she was also the subject of her research all along.
Wendy tells about how a single workout session increases neurotransmitters, like dopamine and serotonin our brains.
It enhances the focusing power of our brain for two hours. To get long-lasting effects, we have increased the duration of the workout.
Regular exercise improves our memory and focuses on work.

## 7. What are some steps you can take to exercise more?

Enumerated are few tips to exercise more often:
- Whenever possible make use of the stairs.
- Daily exercise/gym.
- Make a record of physical activity like bench press, deadlift, sqauts.
- Getting a nutriosious meal.