# Good Practices for Software Development

## 1. What is your one major takeaway from each one of the 6 sections. So 6 points in total.

1. Sometimes, even if you try your best, the requirements can still be unclear. During the implementation phase, it's essential to receive regular feedback to ensure you're on the right track and everyone understands the project's direction. This way, you can avoid misunderstandings and stay aligned with the project's goals.

2. Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.

3. Look at the way issues get reported in large open-source projects

4. Join the meetings 5-10 mins early to get some time with your team members

5. Remember they have their own work to do as well. Pick and choose your communication medium depending on the situation

6. Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps

## 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

* I have to improve my React skills quickly, especially regarding class components.

* I have to regularly practice older topics so as not to get out of touch with them.

* I have to improve on my programming best practices and work on open source projects.

* I will be updating myself regularly on the internet related to the latest JavaScript and React features.
