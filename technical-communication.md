# OSI Model

* In the most basic form, two computers (end-points) connected to each other with LAN Cable (Network Media) and connectors (RJ45) sharing data with the help of NICs forms a network.
But if one computer is based on Microsoft Windows and the other one has MAC OS installed, then how these two computers are going to communicate with each other?
In order to accomplish successful communication b/w computers or networks of different architectures, 7-layered OSI Model or Open System Interconnection Model was introduced by the International Organization for Standardization in 1984. 

* OSI stands for Open System Interconnection is a reference model that describes how information from a software application in one computer moves through a physical medium to the software application in another computer.

**OSI was introduced in 1983 by representatives of the major computer and telecom companies, and was adopted by ISO(International Organization for Standardization) as an international standard in 1984.** 

<picture>
  <img alt="Characteristics of OSI Model" src="https://static.javatpoint.com/tutorial/computer-network/images/osi-model.png">
</picture>

## **7 Layers of OSI Model**

There are the seven OSI layers. Each layer has different functions. A list of seven layers are given below:
   1. Physical Layer
   2. Data-Link Layer
   3. Network Layer
   4. Transport Layer
   5. Session Layer
   6. Presentation Layer
   7. Application Layer 

### **1. Physical Layer**
The physical layer is responsible for the physical cable or wireless connection between network nodes. It defines the connector, the electrical cable or wireless technology connecting the devices, and is responsible for transmission of the raw data, which is simply a series of 0s and 1s, while taking care of bit rate control.

### **2. Data-Link Layer** 
The data link layer establishes and terminates a connection between two physically-connected nodes on a network. It breaks up packets into frames and sends them from source to destination. This layer is composed of two parts—Logical Link Control (LLC), which identifies network protocols, performs error checking and synchronizes frames, and Media Access Control (MAC) which uses MAC addresses to connect devices and define permissions to transmit and receive data.

### **3. Network Layer**
The network layer has two main functions. One is breaking up segments into network packets, and reassembling the packets on the receiving end. The other is routing packets by discovering the best path across a physical network. The network layer uses network addresses (typically Internet Protocol addresses) to route packets to a destination node.

### **4. Transport Layer**
The transport layer takes data transferred in the session layer and breaks it into “segments” on the transmitting end. It is responsible for reassembling the segments on the receiving end, turning it back into data that can be used by the session layer. The transport layer carries out flow control, sending data at a rate that matches the connection speed of the receiving device, and error control, checking if data was received incorrectly and if not, requesting it again.

### **5. Session Layer**
The session layer creates communication channels, called sessions, between devices. It is responsible for opening sessions, ensuring they remain open and functional while data is being transferred, and closing them when communication ends. The session layer can also set checkpoints during a data transfer—if the session is interrupted, devices can resume data transfer from the last checkpoint.

### **6. Presentation Layer**

The presentation layer prepares data for the application layer. It defines how two devices should encode, encrypt, and compress data so it is received correctly on the other end. The presentation layer takes any data transmitted by the application layer and prepares it for transmission over the session layer.

### **7. Application Layer**

The application layer is used by end-user software such as web browsers and email clients. It provides protocols that allow software to send and receive information and present meaningful data to users. A few examples of application layer protocols are the Hypertext Transfer Protocol (HTTP), File Transfer Protocol (FTP), Post Office Protocol (POP), Simple Mail Transfer Protocol (SMTP), and Domain Name System (DNS).

## Advantages of the OSI Model :
___
- It's considered a standard model in computer networking.
- The model supports connectionless, as well as connection-oriented, services. Users can take advantage of connectionless services when they need faster data transmissions over the internet and the connection-oriented model when they're looking for reliability.
- It has the flexibility to adapt to many protocols.
- The model is more adaptable and secure than having all services bundled in one layer.#

## Disadvantages of the OSI Model :
___
- It doesn't define any particular protocol.
- The session layer, which is used for session management, and the presentation layer, which deals with user interaction, aren't as useful as other layers in the OSI model.
- Some services are duplicated at various layers, such as the transport and data-link layers.
- Layers can't work in parallel; each layer must wait to receive data from the previous layer.

## TCP/IP Model vs OSI Model

<picture>
  <img alt="Difference Between TCP/IP vs OSI Model" src="https://www.imperva.com/learn/wp-content/uploads/sites/13/2020/02/OSI-vs.-TCPIP-models.jpg">
</picture>

- The Transfer Control Protocol/Internet Protocol (TCP/IP) is older than the OSI model and was created by the US Department of Defense (DoD). A key difference between the models is that TCP/IP is simpler, collapsing several OSI layers into one:

    * OSI layers 5, 6, 7 are combined into one Application Layer in TCP/IP
    * OSI layers 1, 2 are combined into one Network Access Layer in TCP/IP – however TCP/IP does not take responsibility for sequencing and acknowledgement functions, leaving these to the underlying transport layer.

- Other important differences:

    * TCP/IP is a functional model designed to solve specific communication problems, and which is based on specific, standard protocols. OSI is a generic, protocol-independent model intended to describe all forms of network communication.
    * In TCP/IP, most applications use all the layers, while in OSI simple applications do not use all seven layers. Only layers 1, 2 and 3 are mandatory to enable any data communication.

## References
___
* [https://www.youtube.com/watch?v=vv4y_uOneC0](https://www.youtube.com/watch?v=vv4y_uOneC0) 
* [https://www.imperva.com/learn/application-security/osi-model/](https://www.imperva.com/learn/application-security/osi-model/)

* [https://www.techtarget.com/searchnetworking/definition/OSI](
https://www.techtarget.com/searchnetworking/definition/OSI)
