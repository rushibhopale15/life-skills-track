# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

Sexual harassment is any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.
There are generally three forms of sexual harassment behavior, verbal, visual, and physical.

1. **Verbal harassment:**  Verbal harassment includes comments about clothing, a person's body, sexual or gender-based jokes or remarks, requesting sexual favors, or repeatedly asking a person out. It also includes sexual innuendos, threats, spreading rumors about a person's personal or sexual life, or using foul and obscene language.

2. **Visual harassment:** Visual harassment can include posters, drawings or pictures, screensavers cartoons, emails, or texts of a sexual nature.

3. **Physical harassment:** Physical harassment often includes sexual assault, impeding or blocking movement, inappropriate touching such as kissing, hugging, patting, stroking, or rubbing, sexual gesturing, or even leering or staring.

Each of these forms of harassment and be broken down into two categories. Quid pro quo and hostile work environment.

**Quit pro quo:** Quit pro quo means "this for that." This happens when an employer or supervisor uses job rewards or punishments to coerce an employee into a sexual relationship or sexual act. This includes offering the employee raises or promotions or threatening demotions firing or other penalties for not complying with the request.

**hostile:**  A hostile work environment occurs when an employee's behavior interferes with the work performance of another or creates an intimidating or offensive workplace. This usually happens when someone makes repeated sexual comments and make another employee feel so uncomfortable their work performance suffers. Or they decline professional opportunities because it would put them in contact with the harasser.


## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

If you face or witness any incident or repeated incidents of sexual harassment, it is crucial to take appropriate action to address the situation. Here are some steps you can consider:

1. **Stay safe:** If you feel threatened, try to remove yourself from the situation and find a safe place.

2. **Remember the details:** Take note of when and where the incidents happened, and what exactly occurred. If there were others present, try to remember their names.

3. **Speak up or tell someone:** If you feel comfortable, let the person know that their behavior is not okay and that it makes you uncomfortable. If you don't feel safe doing that, find someone you trust, like a friend, family member, or teacher, and tell them what happened.

4. **Get support:** Talk to someone you trust about what you experienced. They can provide advice, comfort, and help you decide what steps to take next. You can also reach out to organizations or helplines that specialize in helping people who have faced harassment.

5. **Keep any evidence:** If you have any messages, emails, or other evidence of the harassment, keep them in case you need them later. They can be helpful if there is an investigation or if you decide to take legal action.

6. **Know your rights:** Learn about the laws in your area that protect against harassment. Understand what rights you have as a victim or witness.

Remember, it's important to prioritize your safety and well-being. If you're not sure what to do or need more help, reach out to someone you trust or seek advice from a professional who can guide you through the process.