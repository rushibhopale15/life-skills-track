# Focus Management

## 1. What is Deep Work ?

- Deep Work is the ability to focus without distraction on a cognitively demanding task
- It allows us to master complicated concepts in lesser time
- Deep Work can be used in any field to excel in that field
- It is also known as 'Hard Focus' 

## 2. Paraphrase all the ideas in the above videos and this one in detail.


- Deep Work is defined as professional activities performed in a distraction-free environment that pushes our cognitive abilites to the limit
- These efforts increase our abilities and instill new values in us
- Intense periods of focus develops Myelin in the brain cells which allows us to think faster and clearer on the subject
- Famous personalities like Bill Gates and J.K Rowling have used deep work which lead to their success
- We should schedule our distractions in order to do deep work
- We should follow a deep work ritual which will train our mind to do deep work
- Deep work is very rare quality to have
- Early morning is the best time for deep work as distractions are minimum at that time
- Another way to cultivate deep work is to have an evening shutdown routine in order to take rest and get ready for deep work the following day

## 3. How can you implement the principles in your day to day life?

I can implement these principles in the following ways:
- The use of mobile phone while working on something can be minimised
- I can minimise distractions and noise while working
- I can try to focus harder on my work
- I can follow a evening shutdown routine as explained in the book

## 4. Your key takeaways from the video 

My takeaways from this video are:
- Social Media wastes too much time and is unhealthy for mind and body.
- Social Media keeps distracting as it is designed to keep your attention for as long as possible.
- Use of social media is bad for one's mental health as it leads to comparison of lifestyles which can depress a person.
