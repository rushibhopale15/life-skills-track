# Learning Process

## 1. What is the Feynman Technique? Paraphrase the video in your own words.

According to Feynman the best way to learn any concept is by explaining a concept in any area. If a concept is not clear, this technique is very useful to clarify the concept. This technique tests your level of understanding of a concept.

1. Choose a concept to learn. Select a topic you’re interested in learning about and write it at the top of a blank page in a notebook.

2. Teach it to yourself or someone else. Write everything you know about a topic out as if you were explaining it to yourself. Alternately, actually teach it to someone else.

3. Return to the source material if you get stuck. Go back to whatever you’re learning from – a book, lecture notes, podcast – and fill the gaps in your knowledge.

4. Simplify your explanations and create analogies. Streamline your notes and explanation, further clarifying the topic until it seems obvious. Additionally, think of analogies that feel intuitive.

## 2. What are the different ways to implement this technique in your learning process?
There are several ways to implement the Feynman Technique in your learning process. Here are a few approaches:

* Try to Teach a friend or study group.
* Record yourself explaining the concept.
* Pretend to teach the concept to an imaginary audience.
* Try to Practice this technique by mirror practice method.
* Test your understanding through practice questions or quizzes.

## 3. Paraphrase the video in detail in your own words.

Barbara Oakley elplain the some key thing whole video.

* In Women life there are lots of difficulties come during the learning periode some peaple step down but some peaple face that and change the way of finding that goal they take time but get that as well.
* Learn effectively.
* Be fuccussed on our goal where we have to go.
* As Devloper we are work on new project but some time project comes like we have no idea about that so at that situation we have solve some problem that we already know for boosting our confidance and then try that project for atleast 20 to 30 min without any kind of disturbance .
* We learn a lot but some thing that is not use in our life so effective study is one the best thing to grow our skill and do the work in less time.
* And exercise increase the ability to learn something.

## 4. What are some of the steps that you can take to improve your learning process?

1. After Watching this video I got the idea how  we can learn effectively, And Be focuss on our life goal.
2. Keep improve our coding skill by daily exercise like - Home Work , Problem solve on many coding platforms.
3. Always ready for learn new technologies.
4. Learn from mistakes and does not repeat it again.
5. Learn only required thing for any kind of our project and use that very effectively.

## 5. Your key takeaways from the video? Paraphrase your understanding.

You can learn any new skill by investing 20 hours time with more focus. That means 45 minutes a day for a month.

Following are the steps to learn any skill in 20 hours.

**Step 1** : Deconstruct the topic
* Decide what you want to do with that skill after learning.
* Analyse that skill and break it into smaller pieces.
* Select the parts of the skill that helps you and start practicing on those pieces.
* By practicing the most important things first, we will be able to improve our performance.

**Step 2** : Learn enough to self-correct
* Collect  3 to 5 resources on what you want to learn.
* start practicing when you learn just enough this will leads to self-correction of your learning.
* By noticing the mistakes and correcting them we become better at that skill.

**Step 3** : Remove practice barriers
* Avoid the things that distract you while learning.
* Feel confident while learning new things.

**Step 4** : Practice at least 20 hours
* overcome the initial frustration barrier.
* stick to consistent practice. 

The major barrier to learning is not intellectual it's emotional scarceness.

## 6. What are some of the steps that you can while approaching a new topic?

While approaching a new topic I follow the below steps
* Collect the resources that help to learn.
* Start reading basic stuff to understand that new topic.
* Look into the topics and select topics which are helpful to understand that total topic.
* Start practicing with the trial and error method and refer to the resources if you are stuck any where.
* Recall the old stuff before starting new concepts.
* Practice regularly to under the topic.