# Grit and Growth Mindset

## 1. Paraphrase (summarize) the video in a few lines. Use your own words.


- Here is Grit by Angela Duck worth, which is a hope for them. The video is about zeal and hard work in achievement of goal. The author in her research on children found that the I.Q. a gold standard for prediction of intellectual excellence falls short when one works with passion and persistence.

## 2. What are your key takeaways from the video to take action on?

The key takeaways from the video to take action on are:

1. Developing grit in my personality.
2. Believing that failure is not a permanent condition.
3. Learn from my mistakes and keep moving forward.
4. Believing that my ability to learn can change with my effort.
5. Living life like it is a marathon and not a sprint.

## 3. Paraphrase (summarize) the video in a few lines in your own words.

- People with a growth mindset believe their abilities and intelligence can be developed and improved through perseverance, good learning strategies and support from others. They are focused on learning rather than demonstrating their intelligence, so they pursue challenges and persevere in the face of difficulty.

## 4. What are your key takeaways from the video to take action on?

The key takeaways from the video to take action on are:

1. You believe that achievements are down to effort, not just inherent talent.
2. You believe that your intelligence and ability can be developed.
3. You're willing to ask questions and admit when you don't know something.
4. You're willing to learn from your mistakes and find value in criticism.

## 5. What is the Internal Locus of Control? What is the key point in the video?

- Internal locus of control means that control comes from within. You have personal control over your own behavior. When you have an internal locus of control, you believe you have personal agency over your own life and actions. Because of this, these people tend to have more self-efficacy.

## 6. Paraphrase (summarize) the video in a few lines in your own words.

- The mindset stands in contrast to a “fixed mindset,” in which a person believes they possess a certain set of characteristics that’s unlikely to change. In the workplace, these two mindsets can appear both on an individual and at an organizational level.

## 7. What are your key takeaways from the video to take action on?

The key takeaways from the video to take action on are:

1. Identify your own mindset.
2. Make mistakes.
3. Review the success of others.
4. Be kind to yourself.
5. Harness the power of 'yet'.
6. Seek feedback.
7. Learn something new.
8. Look at your own improvements.

## 8. What are one or more points that you want to take action on from the manual? (Maximum 3)

The point that I want to act on from the manual are as follows:

1. I am 100 per cent responsible for my learning.
2. I know more efforts lead to a better understanding.
3. I will stay relaxed and focused no matter what happens.