# Listening and Active Communication

## What are the steps/strategies to do Active Listening?
Active listening is a communication skill that involves fully focusing on and understanding what the speaker is saying.
Here is steps/strategies to do Active Listening -
1. Avoid getting distracted by your own thoughts. Focus on the speaker and topic instead.
2. Show that you are listening with body language.
3. Try not to interrupt the other person. Let them finish and then respond.
4. Paraphrase what other have said to make sure you are on the same page.
5. If appropriate take notes during important conversations.
6. Use door openers. These are phrases that show your are interested and keep the other person talking.


## According to Fisher's model, what are the key points of Reflective Listening? 
Reflective listening, according to Fisher's model, involves a set of key points aimed at fostering effective communication. These points include:

1. **Repeating**: The listener restates or repeats the speaker's words to confirm understanding and show attentiveness.

2. **Paraphrasing**: The listener rephrases the speaker's message using their own words, reflecting back the main ideas and emotions expressed.

3. **Clarifying**: The listener seeks further clarification by asking questions or requesting additional details to ensure a comprehensive understanding of the speaker's thoughts and feelings.

4. **Summarizing**: The listener provides a concise summary of what has been discussed, highlighting the key points and capturing the overall message.

5. **Empathizing**: The listener tries to understand and acknowledge the speaker's emotions and experiences, showing empathy and creating a safe and supportive environment for open communication.

6. **Validating**: The listener acknowledges the speaker's perspective and validates their feelings, demonstrating respect and appreciation for their thoughts and emotions.

By employing these techniques, reflective listening promotes active engagement, understanding, and effective communication between the listener and the speaker.

## What are the obstacles in your listening process?
* When i am in conversation with someone at that time some Irrelevant ideas come in my mind so that i got destructed.
* I am not able to maintain the concentration on that topic for long time, When speaker takes more time then i started loosing interest on that topic.
* Talking with friends in between the concentration that is also obstacles of active listening 

## What can you do to improve your listening?
* Seat with unknown person to avoid the disturbance during the whole season and do not loose inerest on that topic whatever speaker is speaking just listen carefully and take a notbook for noting some point that i am not able understand, For more interaction ask some question from speaker if he is interested for
reply some point that you are not able to understand.

## When do you switch to Passive communication style in your day to day life?
Passive communication is not standing up for self.
* Example1 :- In office someone keep asking project question again and again at that time you are not able say sorry i do not know the answer.
* Example2:- When I am going to purchase something i am not satisfied with the product but still i take that.

## When do you switch into Aggressive communication styles in your day to day life?
Aggressive communication is standing up for self and not respecting the other person-
* When someone is talking with disrespectfully at that time may be i switch to aggressive communication, Else i am calm person so i avoid that kind of situation.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
In case of passive aggressive communication showing sarcasm or talking behind someone back.
* Example:- when i am private discussion at that time, Most of the time this happens with my friends.
## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)
* Learn to recognize and name you feelings and need.
* Be aware of your body what body demands.
* Do not wait for something you need.